import random
import re
import numpy as np
import pandas as pd
import market_data

# TODO: Implement buffer
def get_market_book(ticker):
    if not re.match("[a-z]+", ticker.lower()):
        raise Exception(f"Ticker '{ticker}' invalid!")
    return calculate_book(ticker)


def calculate_book(ticker):
    mid_price = get_random_price_from_ticker_name(ticker)
    bid_depth, ask_depth = random.randint(3, 9), random.randint(3, 9)
    quotes = ["BID"] * bid_depth + ["ASK"] * ask_depth
    price_devs_bid = np.cumsum(np.random.rand(bid_depth) / 100 + .01)[::-1] * -1
    price_devs_ask = np.cumsum(np.random.rand(ask_depth) / 100 + .01)
    prices = np.concatenate((price_devs_bid, price_devs_ask)) + mid_price
    volumes = np.random.randint(1, 100, bid_depth + ask_depth)
    book = pd.DataFrame({"side": quotes,
                         "price": np.round(prices, 3),
                         "volume": volumes},
                            index=list(range(len(prices))))
    book.index.name = "id"
    return book


def execute_order(ticker, volume):
    if np.random.rand() < .75:
        status = True
        message = "OK"
    else:
        status = False
        message = "Execution failed, connection timed out!"
    return status, message

def get_random_price_from_ticker_name(ticker):
    ord(ticker[0]) + random.gauss(0, ord(ticker[0]) / 5.)
