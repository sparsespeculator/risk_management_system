import pytest
import sqlite3
import pandas as pd
import numpy as np
import re
from pandas_datareader import data


if __name__ == "__main__":
    # https://pydata.github.io/pandas-datareader/stable/remote_data.html
    # https://www.learndatasci.com/tutorials/python-finance-part-yahoo-finance-api-pandas-matplotlib/

    result = data.DataReader(["AAPL", "GOOG", "IBM"], 'iex', '2016-01-01', '2018-10-30').reset_index().head()
    result.shape

